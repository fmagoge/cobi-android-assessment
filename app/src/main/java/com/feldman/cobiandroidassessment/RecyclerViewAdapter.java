package com.feldman.cobiandroidassessment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by feldman.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {

    private ArrayList<ItemModel> modelArrayList;
    private ImageLoader imageLoader;
    private Context context;
    private ArrayList<HashMap<String, String>> dataMapArrayList;
    private HashMap<String, String> resultMap = new HashMap<>();
    public OnItemClickListener onItemClickListener;
    NetworkImageView androidIMGGlobal;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        onItemClickListener = listener;
    }

    public static  class RecyclerViewHolder extends RecyclerView.ViewHolder{
        public TextView androidName;
        public TextView androidVersion;
        public TextView androidReleaseDate;
        public TextView androidAPI;
        public final NetworkImageView androidIMG;

        public RecyclerViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);
            androidName = itemView.findViewById(R.id.android_name_card);
            androidVersion = itemView.findViewById(R.id.android_version_card);
            androidReleaseDate =  itemView.findViewById(R.id.version_release_date_card);
            androidAPI = itemView.findViewById(R.id.api_card);
            androidIMG =  itemView.findViewById(R.id.android_ver_img_card);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public RecyclerViewAdapter( ArrayList<HashMap<String, String>> arraylist, Context context) {
        this.dataMapArrayList = arraylist;
        this.context = context;

        imageLoader = CustomVolleyRequestQueue.getInstance(context).getImageLoader();
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_view,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view, onItemClickListener);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {

        resultMap = dataMapArrayList.get(position);
        holder.androidName.setText(resultMap.get(MainActivity.NAME));
        holder.androidVersion.setText(resultMap.get(MainActivity.VERSION));
        holder.androidReleaseDate.setText(resultMap.get(MainActivity.RELEASE));
        holder.androidAPI.setText(resultMap.get(MainActivity.API));
        imageLoader.get(MainActivity.IMAGE_ROOT+resultMap.get(MainActivity.IMAGE), ImageLoader.getImageListener(holder.androidIMG, R.drawable.loading, R.mipmap.ic_launcher));
        holder.androidIMG.setImageUrl(MainActivity.IMAGE_ROOT+resultMap.get(MainActivity.IMAGE), imageLoader);

         androidIMGGlobal = holder.androidIMG;
    }

    @Override
    public int getItemCount() {
        return dataMapArrayList.size();
    }

    public void callBack(int position){
        resultMap = dataMapArrayList.get(position);
        //This intent is going to be used to view the VersionDetailsActivity
        Intent intent = new Intent(context, VersionDetailsActivity.class);
        intent.putExtra("androidName", resultMap.get(MainActivity.NAME));
        intent.putExtra("androidVersion", resultMap.get(MainActivity.VERSION));
        intent.putExtra("androidReleaseDate",resultMap.get(MainActivity.RELEASE));
        intent.putExtra("androidAPI", resultMap.get(MainActivity.API));

        BitmapDrawable drawable = (BitmapDrawable) androidIMGGlobal.getDrawable();
        Bitmap bitmap = drawable.getBitmap();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteAndroImg = stream.toByteArray();
        intent.putExtra("androidIMG",byteAndroImg);

        context.startActivity(intent);
    }


}
