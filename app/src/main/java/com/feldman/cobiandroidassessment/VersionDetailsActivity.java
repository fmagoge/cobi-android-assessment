package com.feldman.cobiandroidassessment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by feldman.
 */
public class VersionDetailsActivity extends Activity {
    String name;
    String version;
    String releaseDate;
    String api;
    String androidImg;
    //ImageLoader imageLoader = new ImageLoader(this);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_version_details);

        Intent i = getIntent();
        name = i.getStringExtra("androidName");
        version = i.getStringExtra("androidVersion");
        releaseDate = i.getStringExtra("androidReleaseDate");
        api = i.getStringExtra("androidAPI");
        androidImg = i.getStringExtra("androidIMG");

        byte[] bytes = getIntent().getByteArrayExtra("androidIMG");
        Bitmap imageBmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

        TextView txtName = (TextView) findViewById(R.id.android_name);
        TextView txtVersion = (TextView) findViewById(R.id.android_version);
        TextView txtReleaseDate = (TextView) findViewById(R.id.version_release_date);
        TextView txtApi = (TextView)findViewById(R.id.androidAPI);

        ImageView img_Andro = (ImageView) findViewById(R.id.android_ver_img);

        txtName.setText(name);
        txtVersion.setText(version);
        txtReleaseDate.setText(releaseDate);
        txtApi.setText(api);
        img_Andro.setImageBitmap(imageBmp);

    }
}
