package com.feldman.cobiandroidassessment;

/**
 * Created by feldman .
 */

public class ItemModel {

    String androidNameText;
    String androidVersionText;
    String androidReleaseDateText;
    String androidAPIText;
    int androidIMG;

    public ItemModel(String androidNameText, String androidVersionText, String androidReleaseDateText, String androidAPIText, int androidIMG) {
        this.androidNameText = androidNameText;
        this.androidVersionText = androidVersionText;
        this.androidReleaseDateText = androidReleaseDateText;
        this.androidAPIText = androidAPIText;
        this.androidIMG = androidIMG;
    }

    public String getAndroidNameText() {
        return androidNameText;
    }

    public void setAndroidNameText(String androidNameText) {
        this.androidNameText = androidNameText;
    }

    public String getAndroidVersionText() {
        return androidVersionText;
    }

    public void setAndroidVersionText(String androidVersionText) {
        this.androidVersionText = androidVersionText;
    }

    public String getAndroidReleaseDateText() {
        return androidReleaseDateText;
    }

    public void setAndroidReleaseDateText(String androidReleaseDateText) {
        this.androidReleaseDateText = androidReleaseDateText;
    }

    public String getAndroidAPIText() {
        return androidAPIText;
    }

    public void setAndroidAPIText(String androidAPIText) {
        this.androidAPIText = androidAPIText;
    }

    public int getAndroidIMG() {
        return androidIMG;
    }

    public void setAndroidIMG(int androidIMG) {
        this.androidIMG = androidIMG;
    }
}
