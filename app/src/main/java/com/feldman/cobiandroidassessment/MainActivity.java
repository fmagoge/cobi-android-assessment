package com.feldman.cobiandroidassessment;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by feldman.
 */
public class MainActivity extends AppCompatActivity {

    private static String file_url = "http://codetest.cobi.co.za/androids.json";
    public static String IMAGE_ROOT = "http://codetest.cobi.co.za/";
    private RecyclerView recyclerView;
    private RecyclerViewAdapter recyclerViewAdapter;
    private RecyclerView.LayoutManager recyclerViewLayoutManager;

    static String NAME = "name";
    static String VERSION = "version";
    static String RELEASE = "released";
    static String API = "api";
    static String IMAGE = "image";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(false);
        recyclerViewLayoutManager = new LinearLayoutManager(this);
        Button buttonLoadAndroidVersions =  findViewById(R.id.buttonLoadVersioins);
        buttonLoadAndroidVersions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DownloadFileFromURL().getFile();
            }
        });
    }

    /**
     * Method to give toast upon completion of downloading the file
     */
    private void downLoadCompleteToast() {
        Toast.makeText(this, "Download complete", Toast.LENGTH_LONG).show();
    }

    /**
     * Inner class responsible for downloading the file from the URL
     */
    class DownloadFileFromURL {

        /**
         * Method to make the request and download the file
         */
        void getFile() {
            String mUrl = file_url;
            InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, mUrl,
                    new Response.Listener<byte[]>() {
                        @Override
                        public void onResponse(byte[] response) {

                            try {
                                if (response != null) {

                                    FileOutputStream outputStream;
                                    String name = "androids.json";
                                    outputStream = openFileOutput(name, Context.MODE_PRIVATE);
                                    outputStream.write(response);
                                    outputStream.close();
                                    downLoadCompleteToast();

                                    /*adapter = new CustomListAdapter(MainActivity.this,getJsonArrayList(getJsonString()));
                                    listView.setAdapter(adapter);*/


                                    recyclerViewAdapter = new RecyclerViewAdapter(getJsonArrayList(getJsonString()),MainActivity.this);
                                    recyclerView.setLayoutManager(recyclerViewLayoutManager);
                                    recyclerView.setAdapter(recyclerViewAdapter);

                                    recyclerViewAdapter.setOnItemClickListener(new RecyclerViewAdapter.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(int position) {
                                            recyclerViewAdapter.callBack(position);
                                        }
                                    });
                                }
                            } catch (Exception e) {

                                Log.d("KEY_ERROR", "UNABLE TO DOWNLOAD FILE");
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    error.printStackTrace();
                }
            }, null);
            RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext(), new HurlStack());
            mRequestQueue.add(request);
        }
    }


    /**
     * Method to retrieve  JsonArrayList
     * @param jsonString String
     * @return ArrayList<HashMap<String, String>>
     */
    public ArrayList<HashMap<String, String>> getJsonArrayList(String jsonString) {

        try {
            JSONObject obj = new JSONObject(jsonString);
            JSONArray jsonArray = obj.getJSONArray("versions");
            ArrayList<HashMap<String, String>> mapArrayList = new ArrayList<HashMap<String, String>>();


            for (int i = 0; i < jsonArray.length(); i++) {
                HashMap<String, String> keyValueHashMap = new HashMap<String, String>();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Iterator key = jsonObject.keys();

                while (key.hasNext()) {
                    String k = key.next().toString();
                    keyValueHashMap.put(k, jsonObject.getString(k));

                }
                mapArrayList.add(keyValueHashMap);

            }

            return mapArrayList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Method to return json string from downloaded file
     * @return String
     */
    public String getJsonString() {
        String json = null;
        try {
            InputStream is = new FileInputStream(Environment.getExternalStorageDirectory().getAbsolutePath() + "/androids.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.d(this.getClass().getName(), "STRING: "+json);
        return json;
    }

}

